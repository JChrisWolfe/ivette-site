
# Project for Alcholics Anoymous of El Paso, Texas

- People Involved

- Project Description

- Requirements

- Deliverables

- Time Frame

- Constraints

# People Involved

- Ivette Mendoza

- Jose Mendoza

- James Christopher Wolfe

- .

# Project Description

The AA group in El Paso needs a visual upgrade to their site. They want to be able to enjoy the new web technologies while still having an accessible freindly site. They would like all the content to be there still but are allowing for some creative and new way to display all the current content they have on their website.

# Requirements

The site will be reviewed as needed until Ivette and the AA group think the site's layout looks appealing for new users.

# Deliverables

On delivery the source code will be provided to Ivette and Jose to be uploaded to the web server.

# Time Frame

Due to the fact that the work is pro-bono there is a lax time window. 

# Constraints

The work is to be done pro-bono. The code might be subject to copy right and not be reshared. The design might need to be changed.

